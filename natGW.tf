resource "aws_eip" "quincusNGeip" {
  vpc = true
}


resource "aws_nat_gateway" "natgw" {
  allocation_id = aws_eip.quincusNGeip.id
  subnet_id     = aws_subnet.quincusPrivate.id

  tags = {
    Name = var.natName
  }
}
