resource "aws_route_table_association" "privateRoutetable_associate" {
    subnet_id = aws_subnet.quincusPrivate.id
    route_table_id = aws_route_table.my_private_route.id
}
