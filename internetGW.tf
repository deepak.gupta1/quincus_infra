resource "aws_internet_gateway" "quincus_vpc_igw" {
  vpc_id = aws_vpc.quincus.id

  tags = {
    Name = var.igName
  }
}
