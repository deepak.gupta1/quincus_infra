variable "vpcId" {
  default = "172.16.0.0/16"
}

variable "vpcName" {
  default = "Quincus_Infra"
}

variable "subnetCidr" {
  default = "172.16.1.0/24"
}

variable "subnetName" {
  default = "QuincusPubSubnet"
}

variable "privateSubnetCidr" {
  default = "172.16.2.0/24"
}

variable "privateSubnetName" {
  default = "QuincusPriSubnet"
}

variable "igName" {
  default = "QuincusIG"
}

variable "routeName" {
  default = "QuincusRT"
}
variable "natName" {
  default = "QuincusNG"
}
variable "prirouteName" {
  default = "QuincusPrivateRT"
}

variable "pubrouteName" {
  default = "QuincusPublicRT"
}
