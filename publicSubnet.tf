resource "aws_subnet" "quincusPublic" {
  vpc_id     = aws_vpc.quincus.id
  cidr_block = var.subnetCidr

  tags = {
    Name = var.subnetName
  }
}
