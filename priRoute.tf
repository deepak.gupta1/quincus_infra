resource "aws_route_table" "my_private_route" {
    vpc_id = aws_vpc.quincus.id

    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = aws_nat_gateway.natgw.id
    }

    tags = {
        Name = var.prirouteName
    }
}
