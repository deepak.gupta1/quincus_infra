resource "aws_subnet" "quincusPrivate" {
  vpc_id     = aws_vpc.quincus.id
  cidr_block = var.privateSubnetCidr
  availability_zone = "ap-south-1b"

  tags = {
    Name = var.privateSubnetName
  }
}
