resource "aws_route_table" "my_public_route" {
    vpc_id = aws_vpc.quincus.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.quincus_vpc_igw.id
    }

    tags = {
        Name = var.pubrouteName
    }
}
