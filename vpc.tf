resource "aws_vpc" "quincus" {
  cidr_block       = var.vpcId
  instance_tenancy = "default"

  tags = {
    Name = var.vpcName
  }
}

output "VPC_ID" {
  value = aws_vpc.quincus.cidr_block
}
