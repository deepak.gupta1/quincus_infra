resource "aws_route_table_association" "routetable_associate" {
    subnet_id = aws_subnet.quincusPublic.id
    route_table_id = aws_route_table.my_public_route.id
}
